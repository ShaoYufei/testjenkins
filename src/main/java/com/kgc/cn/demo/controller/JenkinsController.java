package com.kgc.cn.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class JenkinsController {

    @GetMapping("/jenkins")
    public String test(){
        return "hello jenkins???";
    }
}
